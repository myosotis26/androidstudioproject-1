package com.example.dana.androidstudioproject1;

import android.support.v4.util.PatternsCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.regex.Pattern;

public class CreateTeacherAccountActivity extends AppCompatActivity  {

    private EditText teacherFirstName;
    private EditText teacherLastName;
    private EditText teacherEmail;
    private EditText confirmTeacherEmail;
    private EditText teacherPassword;
    private EditText confirmTeacherPassword;
    private Button signUp;
    private boolean succes;

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 6 characters
                    "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        succes = true;

        teacherFirstName = findViewById(R.id.teacher_first_name);
        teacherLastName = findViewById(R.id.teacher_last_name);
        teacherEmail = findViewById(R.id.teacher_email);
        confirmTeacherEmail = findViewById(R.id.confirm_teacher_email);
        teacherPassword = findViewById(R.id.teacher_password);
        confirmTeacherPassword = findViewById(R.id.confirm_teacher_password);
        signUp = findViewById(R.id.signup_button);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstNameInput = teacherFirstName.getText().toString().trim();
                String lastNameInput = teacherLastName.getText().toString().trim();

                String emailInput = teacherEmail.getText().toString().trim();
                String passwordInput = teacherPassword.getText().toString().trim();

                String confirmEmailInput=confirmTeacherEmail.getText().toString().trim();
                String confirmPasswordInput=confirmTeacherPassword.getText().toString().trim();

                if(firstNameInput.isEmpty()){
                    teacherFirstName.setError("Field can't be empty.");
                    succes = false;
                }
                if(lastNameInput.isEmpty()){
                    teacherLastName.setError("Field can't be empty.");
                    succes = false;
                }

                if (emailInput.isEmpty()) {
                    teacherEmail.setError("Field can't be empty.");
                    succes = false;
                }else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
                    teacherEmail.setError("Please enter a valid email.");
                    succes = false;
                }else if(!(confirmEmailInput.equals(emailInput) && !(confirmEmailInput.isEmpty()) && !(emailInput.isEmpty()))){
                    confirmTeacherEmail.setError("The emails don't match.");
                    teacherEmail.setError(null);
                    succes = false;
                }

                if (passwordInput.isEmpty()) {
                    teacherPassword.setError("Field can't be empty.");
                    succes = false;
                } else if (passwordInput.length() < 6){
                    teacherPassword.setError("Please use at least 6 or more characters.");
                    succes = false;
                } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
                    teacherPassword.setError("The password must contain at least " +
                            "one digit, one upper case letter and one special character. ");
                    succes = false;
                } else if(!(confirmPasswordInput.equals(passwordInput) && !(confirmPasswordInput.isEmpty()) && !(passwordInput.isEmpty()))){
                    confirmTeacherPassword.setError("The passwords don't match.");
                    teacherPassword.setError(null);
                    succes = false;
                }

                if(succes){
                    Toast.makeText(getApplicationContext(), "Account succesfully created !", Toast.LENGTH_LONG).show();

                }

            }
        });


        Spinner subjects_spinner = findViewById(R.id.subjects_spinner);

        ArrayAdapter<CharSequence> subjects_adapter = ArrayAdapter.createFromResource(this, R.array.subjects, android.R.layout.simple_spinner_item);
        subjects_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subjects_spinner.setAdapter(subjects_adapter);
        //subjects_spinner.setOnItemClickListener(this);
        }

    }

